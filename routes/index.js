var express = require('express');
var router = express.Router();
var Product = require('../src/model/product');
var Poll = require('../src/model/poll');
var restful = require('node-restful');
var mongoose = restful.mongoose;


router.get('/index', function (req, res) {
    res.sendFile('index.html', {root: './views'})
});

router.get('/main', function (req, res) {
    res.sendFile('questionlist.html', {root: './views'})
});

router.get('/results', function (req, res) {
    res.sendFile('results.html', {root: './views'})
});

router.get('/admin', function (req, res) {
    res.sendFile('admin.html', {root: './views'})
});

router.get('/editquestions', function (req, res) {
    res.sendFile('editquestions.html', {root: './views'})
});

router.get('/test', function (req, res) {
    res.sendFile('test.html', {root: './views'})
});

//router.get('/getUserlist', function (req, res) {
//    var db = req.db;
//    var collection = db.get('users');
//    collection.find({}, {}, function (e, docs) {
//        res.send(JSON.stringify(docs));
//    });
//});

router.get('/getQuestions', function (req, res) {
    var db = req.db;
    var collection = db.get('PollSchema');
    collection.find({}, {}, function (e, docs) {
        res.send(JSON.stringify(docs));
    });
});

router.get('/poll', function (req, res) {
    return Poll.find(function (err, polls) {
        if (!err) {
            return res.send(polls);
        } else {
            return console.log(err);
        }
    });
});

router.get('/poll/:id', function (req, res) {
    return Poll.find({ _id: req.params.id }, function (err, polls) {
        if (!err) {
            return res.send(polls);
        } else {
            return console.log(err);
        }
    });
});

router.get('/poll/visible/:isvisible', function (req, res) {
    return Poll.find({ visible: req.params.isvisible }, function (err, polls) {
        if (!err) {
            return res.send(polls);
        } else {
            return console.log(err);
        }
    });
});

router.post('/poll', function (req, res) {
    console.log(req.body.toString());
    var poll = new Poll(
        {
            question: req.body.question,
            choices: req.body.choices,
            visible: req.body.visible
        }
    );

    poll.save(function (err) {
        if (!err) {
            return console.log("created");
        } else {
            return console.log(err);
        }
    });
    return res.send(poll);
});

router.put('/poll/:id', function (req, res) {
    console.log("poll question no is " + req.params.id);
    try {
        return Poll.findOne({ _id: req.params.id}, function (err, poll) {
            try {
                console.log("visible is " + req.body.visible);
                poll.visible = req.body.visible;
                poll.save(function (err, doc) {
                    if (!err) {
                        return console.log("updated");
                    } else {
                        return console.log(err);
                    }
                });

                return res.json(poll);
            } catch (err) {
                console.log(err);
            }

        });
    } catch (err) {
        console.log(err);
    }
});

router.put('/pushquestion/:id', function (req, res) {
    console.log("poll question no is " + req.params.id);
    try {
        return Poll.findOne({ _id: req.params.id}, function (err, poll) {
            var socketio = req.app.get('socketio');
            socketio.emit("notification", poll._id);
            return res.json(poll);
        });
    } catch (err) {
        console.log(err);
    }
});


router.put('/vote/:id', function (req, res) {
    console.log("poll question no is " + req.params.id);
    try {
        return Poll.findOne({ _id: req.params.id}, function (err, poll) {
            try {
                var ip = req.headers['x-forwarded-for'] ||
                    req.connection.remoteAddress ||
                    req.socket.remoteAddress ||
                    req.connection.socket.remoteAddress;
                console.log("ip is " + ip);
                console.log("choice id is " + req.body.choice);
                var choice = poll.choices.id(req.body.choice);
                choice.votes.push({ ip: ip});
                poll.save(function (err, doc) {
//                    var theDoc = {
//                        question: doc.question, _id: doc._id, choices: doc.choices,
//                        userVoted: false, totalVotes: 0
//                    };

                    var socketio = req.app.get('socketio');
                    socketio.emit("refreshVotes", "Vote Result")

                    if (!err) {
                        return console.log("updated");
                    } else {
                        return console.log(err);
                    }
                });
                return res.json(poll);
            } catch (err) {
                console.log(err);
            }

        });
    } catch (err) {
        console.log(err);
    }
});


router.delete('/poll/:id', function (req, res) {
    Poll.remove({ _id: req.params.id }, function (err) {
        if (!err) {
            console.log("Successfully deleted");
        }
        else {
            console.log("ERROR");
        }
        return res.send();
    });
});


router.get('/pollresult', function (req, res) {
    var pollresults = [];

    Poll.find({}, function (err, questions) {
        try {
            questions.forEach(function (question) {
                var choices = question.choices;
                var choicesCollection = [];
                var voteCount = 0;
                choices.forEach(function (choice) {
                    var choiceObj = {};
                    // Choice.distinct('votes').count().exec(function (err, count) {
                    var count = choice.votes.length;
                    choiceObj.choice = choice.text;
                    choiceObj.totalVotes = count;
                    voteCount += count;
                    choicesCollection.push(choiceObj);
                });
                var theDoc = {
                    _id: question._id, question: question.question, choices: choicesCollection, voteCount: voteCount
                };
                pollresults.push(theDoc);
            });
        } catch (err) {
            console.log(err);
        }
        return res.send(pollresults);
    });
});

router.get('/pollresult/:id', function (req, res) {
    var pollresults = [];

    Poll.findOne({ _id: req.params.id}, function (err, question) {
        try {
            // questions.forEach(function (question) {
            var choices = question.choices;
            var choicesCollection = [];
            var voteCount = 0;
            choices.forEach(function (choice) {
                var choiceObj = {};
                // Choice.distinct('votes').count().exec(function (err, count) {
                var count = choice.votes.length;
                choiceObj.choice = choice.text;
                choiceObj.totalVotes = count;
                voteCount += count;
                choicesCollection.push(choiceObj);
            });
            var theDoc = {
                _id: question._id, question: question.question, choices: choicesCollection, voteCount: voteCount
            };
            pollresults.push(theDoc);
            // });
        } catch (err) {
            console.log(err);
        }
        return res.send(pollresults);
    });
});

module.exports = router;

